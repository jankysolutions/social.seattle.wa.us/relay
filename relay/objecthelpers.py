from urllib.parse import urlsplit

from . import dbhelpers


def distill_inboxes(a, object_id):
    origin_hostname = urlsplit(object_id).hostname

    inbox = get_actor_inbox(a)
    targets = [target for target in dbhelpers.list_followers() if target != inbox]
    targets = [target for target in targets if urlsplit(target).hostname != origin_hostname]
    hostnames = [urlsplit(target).hostname for target in targets]

    assert inbox not in targets
    assert origin_hostname not in hostnames

    return targets


def distill_object_id(activity):
    obj = activity['object']

    if isinstance(obj, str):
        return obj

    return obj['id']


def get_actor_inbox(a):
    return a.get('endpoints', {}).get('sharedInbox', a['inbox'])
