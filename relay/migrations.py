from urllib.parse import urlsplit
import sqlite3
import logging

logger = logging.getLogger(__name__)


ID_ROW = ["id", "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"]

TABLES = {
    "db_migration_version": [
        ["version", "INTEGER PRIMARY KEY NOT NULL"]
    ],
    "instances": [
        ID_ROW,
        ["domain", "TEXT UNIQUE NOT NULL"],
        ["inbox", "TEXT"],
        ["allow_inbound", "BOOL NOT NULL DEFAULT 1"],
        ["allow_outbound", "BOOL NOT NULL DEFAULT 1"]
    ],
    "followers": [
        ID_ROW,
        ["instance", "INTEGER UNIQUE NOT NULL"]
    ],
    "following": [
        ID_ROW,
        ["instance", "INTEGER UNIQUE NOT NULL"],
        ["accepted", "BOOLEAN DEFAULT 0"]
    ],
    "activity": [
        ID_ROW,
        ["timestamp", "TIMESTAMP DEFAULT CURRENT_TIMESTAMP"],
        ["received_from", "TEXT NOT NULL"],
        ["object_domain", "TEXT NOT NULL"],
        ["type", "TEXT NOT NULL"],
        ["cached", "BOOLEAN DEFAULT 0"]
    ],
    "requests": [
        ID_ROW,
        ["timestamp", "TIMESTAMP DEFAULT CURRENT_TIMESTAMP"],
        ["instance", "INTEGER"],
        ["status", "INTEGER"],
        ["elapsed", "INTEGER"]
    ]
}


def add_domain_to_instances(c):
    c.execute("SELECT id, inbox FROM instances")

    updates = []
    domains = []
    logger.info("Preparing instance migration to add 'domain' column")
    for id, inbox in c.fetchall():
        url = urlsplit(inbox)
        if url.scheme == "https":
            updates.append([url.netloc, id])
            logger.info("Instance with inbox %s has domain %s", inbox, url.netloc)
        elif url.scheme == "":
            domains.append([inbox, id])
            logger.info("Instance with inbox %s isn't really a URL, converting that to the domain", inbox)

    c.execute("ALTER TABLE instances ADD COLUMN domain TEXT NOT NULL DEFAULT ''")
    c.executemany("UPDATE instances SET domain = ? WHERE id = ?", updates)

    for domain, id in domains:
        logger.info("Trying to create domain-only instance entry for %s", domain)
        try:
            c.execute("UPDATE instances SET domain = ?, inbox = NULL WHERE id = ?", [domain, id])
        except sqlite3.IntegrityError:
            logger.info("IntegrityError while adding domain-only instance for domain %s, skipping (presumably already inserted with an inbox value), deleting the whole row", domain)
            c.execute("SELECT id FROM instances WHERE domain = ?", [domain])
            new_id = c.fetchone()[0]
            for table in ["requests", "followers", "following"]:
                sql = "UPDATE {} SET instance = ? WHERE instance = ?".format(table)
                logger.info("Executing %s with params %s and %s", sql, new_id, id)
                c.execute(sql, [new_id, id])
            c.execute("DELETE FROM instances WHERE id = ?", [id])
    # c.execute("CREATE UNIQUE INDEX instance_unique_domain ON instances(domain)")


def build_create_table_sql(table):
    columns = [" ".join(c) for c in TABLES[table]]
    return "CREATE TABLE IF NOT EXISTS {} ({})".format(table, ", ".join(columns))


MIGRATIONS = [
    ["INSERT INTO followers(instance) SELECT id FROM instances;"],
    [
        "ALTER TABLE instances RENAME TO _instances_old;",
        "CREATE TABLE instances(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, inbox TEXT NOT NULL);",
        "INSERT INTO instances(id, inbox) SELECT id, instance FROM _instances_old;",
        "DROP TABLE _instances_old;",
    ],
    ["ALTER TABLE following ADD COLUMN accepted BOOLEAN DEFAULT 0"],
    [build_create_table_sql("activity")],
    [build_create_table_sql("requests")],
    add_domain_to_instances,
    [
        "ALTER TABLE instances ADD COLUMN allow_inbound NOT NULL DEFAULT 1",
        "ALTER TABLE instances ADD COLUMN allow_outbound NOT NULL DEFAULT 1"
    ],
    [
        "ALTER TABLE instances RENAME TO _instances_old;",
        "CREATE TABLE instances (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, domain TEXT UNIQUE NOT NULL, inbox TEXT, allow_inbound BOOL NOT NULL DEFAULT 1, allow_outbound BOOL NOT NULL DEFAULT 1)",
        "INSERT INTO instances(id, domain, inbox, allow_inbound, allow_outbound) SELECT id, domain, inbox, allow_inbound, allow_outbound FROM _instances_old;",
        "DROP TABLE _instances_old;",
    ],
]
