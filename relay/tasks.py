from celery import Celery
import json
import requests
import logging
import uuid
import sqlite3
from urllib.parse import urlsplit

from . import actor, config
from .objecthelpers import distill_object_id, distill_inboxes, get_actor_inbox, dbhelpers
from .http_signatures import sign_headers

logger = logging.getLogger(__name__)

app = Celery('tasks', broker=config.get("relay", "broker"))


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def relay(self, data, host, following):
    if not dbhelpers.allow_inbound(data['actor']):
        logger.info("Dropping message from actor %s", data['actor'])
        return
    object_id = distill_object_id(data)

    activity_id = "https://{}/activities/{}".format(host, uuid.uuid4())

    message = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Announce",
        "to": ["https://{}/actor/followers".format(host)],
        "actor": "https://{}/actor".format(host),
        "object": object_id,
        "id": activity_id
    }

    logging.debug('>> relay: %r', message)

    a = actor.fetch(data['actor'])
    inboxes = distill_inboxes(a, object_id)

    for inbox in inboxes:
        push_message_to_actor.delay({'inbox': inbox}, message, "https://{}/actor#main-key".format(host))


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def follow(self, data, host, following):
    if not dbhelpers.allow_inbound(data['actor']):
        logger.info("Not following actor %s (inbound disallowed)")
        return
    a = actor.fetch(data["actor"])
    inbox = get_actor_inbox(a)
    # TODO: check if urlsplit(inbox).hostname is in the list of blocked followers
    if inbox not in following:
        dbhelpers.add_follower(inbox)

    if distill_object_id(data).endswith('/actor'):
        follow_remote_actor.delay(a["id"])

    message = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Accept",
        "to": [a["id"]],
        "actor": "https://{}/actor".format(host),  # TODO: use flask.url_for
        # this is wrong per litepub, but mastodon < 2.4 is not compliant with that profile.
        # TODO: fuck mastodon < 2.4, do this the correct way (whatever that may be)
        "object": {
            "type": "Follow",
            "id": data["id"],
            "object": "https://{}/actor".format(host),  # TODO: use flask.url_for
            "actor": a["id"]
        },
        "id": "https://{}/activities/{}".format(host, uuid.uuid4()),  # TODO: use flask.url_for
    }
    push_message_to_actor(a, message, "https://{}/actor#main-key".format(host))  # TODO: use flask.url_for


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def undo(self, data, host, request):
    child = data['object']
    if child['type'] == 'Follow':
        a = actor.fetch(data['actor'])
        inbox = get_actor_inbox(a)
        logger.info("Request to unfollow %s", inbox)
        removed = dbhelpers.remove_follower(inbox)

        if removed and distill_object_id(child).endswith('/actor'):
            logger.info("Removing follower %s from database", inbox)
            unfollow_remote_actor.delay(a['id'])


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def accept(self, data, host, request):
    dbhelpers.accept_follow(data["actor"])


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def follow_remote_actor(self, actor_uri):
    a = actor.fetch(actor_uri)
    if not a:
        logging.warn('failed to fetch actor at: %r', a)
        return

    logging.info('following: %r', a)

    message = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Follow",
        "to": [a['id']],
        "object": a['id'],
        "id": "https://{}/activities/{}".format(config['relay']['public_hostname'], uuid.uuid4()),
        "actor": "https://{}/actor".format(config['relay']['public_hostname'])
    }
    push_message_to_actor(a, message, "https://{}/actor#main-key".format(config['relay']['public_hostname']))
    dbhelpers.add_following(get_actor_inbox(a))


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def unfollow_remote_actor(self, actor_uri):
    a = actor.fetch(actor_uri)
    if not a:
        logging.info('failed to fetch actor at: %r', a)
        return

    logging.info('unfollowing: %r', a)

    message = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Undo",
        "to": [a['id']],
        "object": {
            "type": "Follow",
            "object": a,
            "actor": a['id'],
            "id": "https://{}/activities/{}".format(config['relay']['public_hostname'], uuid.uuid4())
        },
        "id": "https://{}/activities/{}".format(config['relay']['public_hostname'], uuid.uuid4()),
        "actor": "https://{}/actor".format(config['relay']['public_hostname'])
    }
    push_message_to_actor(a, message, "https://{}/actor#main-key".format(config['relay']['public_hostname']))
    dbhelpers.remove_following(get_actor_inbox(a))


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def push_message_to_actor(self, a, message, our_key_id):
    inbox = get_actor_inbox(a)

    url = urlsplit(inbox)

    # XXX: Digest
    data = json.dumps(message)
    headers = {
        '(request-target)': 'post {}'.format(url.path),
        'Content-Type': 'application/activity+json',
        'Content-Length': str(len(data)),
        'User-Agent': config.get('relay', 'user_agent')
    }
    headers['signature'] = sign_headers(headers, our_key_id)
    headers.pop('(request-target)')

    logger.debug("Posting to %s:\n%s", inbox, data)

    res = requests.post(inbox, data=data, headers=headers, timeout=60)
    logger.debug("Got back HTTP %s: %s", res.status_code, res.content)
    log_request(res)
    res.raise_for_status()


def log_request(response):
    domain = urlsplit(response.url).netloc
    elapsed = response.elapsed.total_seconds() * 1000
    log_request_task.delay(domain, response.status_code, elapsed)


@app.task(bind=True, autoretry_for=(sqlite3.OperationalError,), retry_backoff=True)
def log_request_task(self, domain, status, elapsed):
    dbhelpers.log_request(domain, status, elapsed)


@app.task(bind=True, autoretry_for=(requests.RequestException,), retry_backoff=True)
def fetch_actor(self, actor_uri):
    actor.fetch(actor_uri)


tasks = {
    "Follow": follow,
    "Announce": relay,
    "Create": relay,
    "Undo": undo,
    "Accept": accept
}
