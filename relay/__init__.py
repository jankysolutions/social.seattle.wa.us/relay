import configparser
import os
import codecs
import logging
from Crypto.PublicKey import RSA
from urllib.parse import urlsplit

from .get_version import get_version

version = get_version()

logger = logging.getLogger(__name__)

config = configparser.ConfigParser()
# The default config
config['relay'] = {
    "broker": "redis://localhost",
    "sqlite_db": "relay.db",
    "base_url": "http://localhost:5000",
    "public_hostname": "relay.example.com",
    "log_level": "INFO"
}
config.read([os.environ.get("CONFIG", "relay.conf"), "/etc/relay.conf"])

config_updated = False

if "token" not in config['relay']:
    logger.warn("No token specified! Generating one...")
    config['relay']['token'] = codecs.encode(os.urandom(16), 'base64').strip().decode('utf-8')
    config_updated = True

if "private_key" not in config['relay']:
    logger.warn("No private key in config, generating one")
    privkey = RSA.generate(4096)
    config['relay']['private_key'] = privkey.exportKey('PEM').decode('utf-8')
    config['relay']['public_key'] = privkey.publickey().exportKey('PEM').decode('utf-8')
    logger.info("key pair generated")
    config_updated = True


if config['relay']['public_hostname'] == "relay.example.com" and "public_url" in config['relay']:
    config['relay']['public_hostname'] = urlsplit(config['relay']['public_url']).netloc
    del config['relay']['public_url']
    config_updated = True

if config_updated:
    logger.info("Saving config to relay.conf")
    with open('relay.conf', 'w') as f:
        config.write(f)

logging.basicConfig(level=logging.getLevelName(config['relay']['log_level']))

config['relay']['user_agent'] = "Janky Solutions Federation Relay ({}); +https://{};".format(version, config['relay']['public_hostname'])
