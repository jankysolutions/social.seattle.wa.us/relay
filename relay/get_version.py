import subprocess


def get_version():
    version = "dev"

    try:
        describe = subprocess.run(["git", "describe", "--always", "--dirty"], stdout=subprocess.PIPE, check=True)
        version = describe.stdout.strip().decode('utf-8')
    except Exception:
        pass

    return version
