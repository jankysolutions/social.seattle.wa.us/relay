import sqlite3  # right now we're sqlite only. "real" database support will be added when *YOU* (yes, you!) write it
from flask import g
import logging
from urllib.parse import urlsplit

from . import config
from .migrations import MIGRATIONS, TABLES, build_create_table_sql

logger = logging.getLogger(__name__)


def get_db(**kwargs):
    if g:
        db = getattr(g, '_database', None)
        if db is None:
            db = g._database = sqlite3.connect(config.get('relay', 'sqlite_db'))
        return db
    else:
        return sqlite3.connect(config.get('relay', 'sqlite_db'))


def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def execute(c, query, args=[], commit=False):
    logger.debug("Executing %s with params %s", query, args)
    c.execute(query, args)
    if commit:
        c.connection.commit()
        logger.debug("Committed DB transaction")


def allow_inbound(actor):
    c = get_db().cursor()
    domain = urlsplit(actor).netloc
    # TODO: Also allow blocking inbound from specific actors
    execute(c, "SELECT allow_inbound FROM instances WHERE domain = ?", [domain])
    results = c.fetchone()
    if results is None:
        return True  # Default permissive, this should be configurable
    return results[0] == 1


def list_followers():
    c = get_db().cursor()
    execute(c, "SELECT instances.inbox FROM instances INNER JOIN followers ON followers.instance = instances.id WHERE instances.allow_outbound = 1;")
    followers = [follower[0] for follower in c.fetchall()]  # c.fetchall() will return a list of tuples with one value each
    return followers


def add_follower(inbox):
    db = get_db()
    c = db.cursor()
    try:
        instance_id = add_instance(inbox)
        execute(c, "INSERT INTO followers(instance) VALUES(?)", [instance_id], True)
        logger.debug("Committed database transaction")
    except sqlite3.IntegrityError:
        logger.info("Refusing to re-add follower %s", inbox)
        db.rollback()
        logger.debug("Rolled back transaction")


def remove_follower(inbox):
    db = get_db()
    c = db.cursor()
    execute(c, "DELETE FROM followers WHERE instance = (SELECT id FROM instances WHERE inbox = ?)", [inbox], True)

    return c.rowcount > 0


def add_following(inbox):
    instance = add_instance(inbox=inbox)
    db = get_db()
    c = db.cursor()
    try:
        execute(c, "INSERT INTO following(instance) VALUES (?)", [instance])
    except sqlite3.IntegrityError:
        logger.warn("%s already in following list", inbox)


def remove_following(inbox):
    instance = add_instance(inbox)
    db = get_db()
    c = db.cursor()
    execute(c, "DELETE FROM following WHERE id = ?", [instance])


def accept_follow(inbox):
    db = get_db()
    c = db.cursor()
    execute(c, "UPDATE following SET accepted = 1 WHERE instance = (SELECT id FROM instances WHERE inbox = ?)", [inbox])


def add_instance(inbox=None, domain=None):
    db = get_db()
    c = db.cursor()

    if inbox is not None and domain is None:
        domain = urlsplit(inbox).netloc

    assert domain is not None

    execute(c, "SELECT id FROM instances WHERE domain = ?", [domain])
    result = c.fetchone()
    if result is not None:
        return result[0]

    execute(c, "INSERT INTO instances(domain, inbox) VALUES(?, ?)", [domain, inbox], True)
    return c.lastrowid


def log_activity(received_from, object_domain, type, cached):
    db = get_db()
    c = db.cursor()
    execute(c, "INSERT INTO activity(received_from, object_domain, type, cached) VALUES(?, ?, ?, ?)",
            [received_from, object_domain, type, cached], True)


def log_request(domain, status, elapsed):
    db = get_db()
    c = db.cursor()
    instance = add_instance(domain=domain)
    execute(c, "INSERT INTO requests(instance, status, elapsed) VALUES(?, ?, ?)", [instance, status, elapsed], True)


def get_stats_for_index():
    c = get_db().cursor()
    results = {}

    execute(c, 'SELECT COUNT(*) AS count, object_domain FROM activity WHERE cached = 0 GROUP BY object_domain ORDER BY count DESC LIMIT 15')
    results['top_domains'] = [{"domain": domain, "count": count} for domain, count in c.fetchall()]

    execute(c, 'SELECT COUNT(*) FROM activity WHERE cached = 0')
    results['unique_messages'] = c.fetchone()[0]

    execute(c, 'SELECT COUNT(*) FROM activity WHERE cached = 1')
    results['duplicate_messages'] = c.fetchone()[0]

    execute(c, 'SELECT instances.id, instances.domain, instances.inbox, instances.allow_outbound, instances.allow_inbound FROM followers INNER JOIN instances ON followers.instance = instances.id;')
    results['followers'] = [{"instance": r[0], "domain": r[1], "inbox": r[2], "allow_outbound": r[3], "allow_inbound": r[4]} for r in c.fetchall()]

    execute(c, "SELECT instance, AVG(elapsed), COUNT(id) FROM requests WHERE requests.timestamp > datetime('now', '-7 days') GROUP BY instance;")

    requests = {}
    for instance, elapsed, count in c.fetchall():
        requests[instance] = {"avg_ms": elapsed, "count": count}

    results['requests'] = requests

    execute(c, "SELECT instances.inbox FROM instances INNER JOIN following ON following.instance = instances.id;")
    results['following'] = [following[0] for following in c.fetchall()]

    execute(c, "SELECT COUNT(*) FROM activity WHERE cached = 0 AND timestamp > datetime('now', '-7 days')")
    results['messages_week'] = c.fetchone()[0]

    execute(c, "SELECT COUNT(*) FROM activity WHERE cached = 0 AND timestamp > datetime('now', '-1 day')")
    results['messages_day'] = c.fetchone()[0]

    return results


def close(db):
    if not g:
        db.close()


def init_db():
    logger.info("Initializing DB")
    db = get_db()
    c = db.cursor()

    try:
        logger.info("Checking current database migration version")
        execute(c, "SELECT version FROM db_migration_version ORDER BY version DESC LIMIT 1;")
    except sqlite3.OperationalError:
        logger.info("No db_migration_version table, building all tables")
        for table in TABLES.keys():
            sql = build_create_table_sql(table)
            execute(c, sql)
        execute(c, "INSERT INTO db_migration_version(version) VALUES(?)", [len(MIGRATIONS)], True)
    else:
        db_migration_version = c.fetchone()[0]
        logger.debug("Database is at version %s, need version %s, performing migrations if needed", db_migration_version, len(MIGRATIONS))
        for upgrade_from in range(db_migration_version, len(MIGRATIONS)):
            upgrade_to = upgrade_from + 1
            logger.warn("Database migration in progress, upgrading %s -> %s", upgrade_from, upgrade_to)
            if callable(MIGRATIONS[upgrade_from]):
                logger.info("Migration %s: running function %s", upgrade_to, MIGRATIONS[upgrade_from].__name__)
                MIGRATIONS[upgrade_from](c)
            else:
                for statement in MIGRATIONS[upgrade_from]:
                    logger.info("Migration %s: %s", upgrade_to, statement)
                    execute(c, statement)
            logger.info("Migration %s: INSERT INTO db_migration_version(version) VALUES(%s)", upgrade_to, upgrade_to)
            execute(c, "INSERT INTO db_migration_version(version) VALUES(?)", [upgrade_to], True)
            logger.warning("Migration %s completed successfully", upgrade_to)

    close(db)
