from urllib.parse import urlsplit
from werkzeug.contrib.cache import RedisCache

from . import config

ONEWEEK = 60 * 60 * 24 * 7

url = urlsplit(config['relay']['broker'])
path = None if url.path.strip('/') == "" else url.path.strip('/')

cache = RedisCache(url.netloc, url.port or 6379, url.password, path, default_timeout=ONEWEEK, key_prefix="cache-")
