import logging
import requests

from . import config
from .cache import cache

logger = logging.getLogger(__name__)


def fetch(uri, force=False):
    cached = cache.get(uri)
    if cached is None or force:
        logger.info("Requesting %s (%s)", uri, "cache bypassed" if force else "not in cache")
        headers = {
            "Accept": "application/activity+json",
            "User-Agent": config.get("relay", "user_agent")
        }
        res = requests.get(uri, headers=headers)
        res.raise_for_status()
        cached = res.json()
        cache.set(uri, cached)
    return cached
