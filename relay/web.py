from flask import Flask, request, Response, abort, jsonify, url_for, render_template, redirect
import click
import requests
import os
import hashlib
from urllib.parse import urlsplit

from . import tasks, dbhelpers, config, http_signatures, version
from .objecthelpers import distill_object_id
from .cache import cache


app = Flask(__name__)
dbhelpers.init_db()


NODEINFO_TEMPLATE = {
    # XXX - is this valid for a relay?
    # TODO: See why the pleroma people said that ^^
    'openRegistrations': True,
    'protocols': ['activitypub'],
    'services': {'inbound': [], 'outbound': []},
    'software': {'name': 'social.seattle.wa.us-relay', 'version': version},
    'usage': {'localPosts': 0, 'users': {'total': 1}},
    'version': '2.0'
}


class ReverseProxied(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        environ['wsgi.url_scheme'] = "https"
        return self.app(environ, start_response)


app.wsgi_app = ReverseProxied(app.wsgi_app)


@app.before_request
def validate():
    if 'signature' in request.headers:
        if 'actor' not in request.json:
            return abort(401, Response("signature check failed, no actor in message"))

        a = request.json["actor"]

        if not cache.has(a):
            # Queue a task to fetch this actor's key. Sometimes it takes too long to request it and the request
            # that we're receiving times out.
            tasks.fetch_actor.delay(a)

        if not http_signatures.validate(a, request):
            app.logger.info("Signature validation failed for: %r", actor)
            return abort(401, Response("signature check failed, signature did not match key"))
    elif request.path in ["/inbox"] and request.method != "GET":
        return abort(401, Response("No signature"))


@app.before_request
def validate_internal():
    if request.path.startswith('/internal'):
        presented_token = request.headers.get('X-Relay-Token', 'invalid_token')
        if presented_token != config.get('relay', 'token'):
            return abort(401)


@app.teardown_appcontext
def close_connection(exception):
    dbhelpers.close_connection(exception)


@app.route('/inbox', methods=["POST"])
def inbox():
    if "actor" not in request.json:
        app.logger.info("rejecting request because 'actor' key missing")
        return abort(400)

    if "type" not in request.json:
        app.logger.info("rejecting request because 'type' key missing")
        return abort(400)

    type = request.json["type"]

    if type not in tasks.tasks:
        app.logger.info("Rejecting unknown task type %s", type)
        return abort(400)

    object_id = distill_object_id(request.json)

    dbhelpers.log_activity(received_from=request.json['actor'],
                           object_domain=urlsplit(object_id).netloc,
                           type=request.json["type"],
                           cached=cache.has(object_id))

    cached = cache.has(object_id)
    if cached and type in ["Announce", "Create"]:  # Other types (follow, unfollow) use the same object_id everywhere
        app.logger.debug("Not scheduling duplicate %s task for object id %s task: %s", type, object_id, request.json)
    else:
        app.logger.debug("Scheduling %s task: %s", type, request.json)
        tasks.tasks[type].delay(request.json, request.host, dbhelpers.list_followers())
        cache.set(object_id, True)
    return Response("{}", "application/activity+json"), 200 if cached else 202


@app.route("/inbox", methods=["GET"])
def inbox_redirect_to_index():
    return redirect(url_for('index'))


@app.route('/actor')
def actor():
    return jsonify({
        "@context": "https://www.w3.org/ns/activitystreams",
        "endpoints": {
            "sharedInbox": url_for('inbox', _external=True)
        },
        "followers": "https://{}/followers".format(request.host),
        "following": "https://{}/following".format(request.host),
        "inbox": url_for('inbox', _external=True),
        "name": "Relay",
        "type": "Application",
        "id": url_for('actor', _external=True),
        "publicKey": {
            "id": url_for('actor', _external=True, _anchor="main-key"),
            "owner": url_for('actor', _external=True),
            "publicKeyPem": config['relay']['public_key']
        },
        "summary": "federation relay bot",
        "preferredUsername": "relay",
        "url": url_for('actor', _external=True)
    })


@app.route("/.well-known/webfinger")
def webfinger():
    subject = request.args['resource']

    if subject != 'acct:relay@{}'.format(request.host):
        return jsonify({"error": "user not found"}), 404

    actor_uri = url_for('actor', _external=True)
    return jsonify({
        "aliases": [actor_uri],
        "links": [
            {"href": actor_uri, "rel": "self", "type": "application/activity+json"},
            {"href": actor_uri, "rel": "self", "type": "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""}
        ],
        "subject": subject
    })


@app.route('/.well-known/nodeinfo')
def nodeinfo():
    return jsonify({
        "links": [
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                "href": url_for("nodeinfo2", _external=True)
            }
        ]
    })


@app.route('/nodeinfo/2.0.json')
def nodeinfo2():
    info = NODEINFO_TEMPLATE.copy()
    info['metadata'] = {"peers": [urlsplit(f).hostname for f in dbhelpers.list_followers()]}
    return jsonify(info)


@app.route("/")
def index():
    return render_template('index.html',
                           version=version,
                           stats=dbhelpers.get_stats_for_index(),
                           public_hostname=config['relay']['public_hostname'])


@app.cli.command()
@click.argument('actor')
def follow(actor):
    tasks.follow_remote_actor(actor)


@app.cli.command()
@click.argument('actor')
def unfollow(actor):
    tasks.unfollow_remote_actor(actor)


@app.cli.command()
def clear_cache():
    cache.clear()


@app.cli.command()
def migrate():
    dbhelpers.init_db()


@app.cli.command()
def download_assets():
    assets = [
        ("bootstrap.css", "https://getbootstrap.com/docs/4.2/dist/css/bootstrap.css", "e54df3f4adcfd7b70a8066315d0039ac164ee440e3bbe960b571baa035cd6cd6"),
        ("bootstrap.min.css", "https://getbootstrap.com/docs/4.2/dist/css/bootstrap.min.css", "6b3bef53dc4a96ec07149d02a60b5fd026332bbce0b4ece79f3c55e3ddb85f5c")
    ]
    for asset in assets:
        path = os.path.join("relay", "static", "vendor", asset[0])
        app.logger.info("Downloading asset %s to %s", asset[1], path)
        r = requests.get(asset[1])
        hash = hashlib.sha256(r.content).hexdigest()
        if hash != asset[2]:
            app.logger.warn("Expected hash %s but got %s when downloadings %s", asset[2], hash, asset[0])
            os.exit(1)

        with open(path, 'wb') as f:
            f.write(r.content)
