# Janky Solutions ActivityPub relay

This is an attempt to build something like the [Pleroma relay](https://git.pleroma.social/pleroma/relay) but with
crazy things like workers. Still got a ways to go, but less bad than before.


## Installation
*A lot of this assumes you know what you're doing, file an issue if you're confused.*

```
virtualenv -p python3 env
env/bin/pip install -r requirements.txt
env/bin/flask migrate
env/bin/flask download-assets  # downloads bootstrap.css to relay/static/vendor/
```

At this point you should have `relay.conf` and `relay.db` files. Edit `relay.conf`,
update `public_hostname` to be the FQDN that this server is publically accessible via HTTPS at.

You will also need to configure a redis server.

## Running
There are two separate processes: the http server and the worker(s).

To run the http server:

```
env/bin/gunicorn relay.web:app
```

To run the worker:
```
env/bin/celery -A relay.tasks.app worker
```

gunicorn and celery both have a wide range of possible arguments, configure as needed.


## Following other relay servers

*Plz don't create endless loops :(, I don't think it would handle that well.*

Why shouldn't relay servers be able to follow each other?

```
env/bin/flask follow https://relay.example.social/inbox
```

and to unfollow:

```
env/bin/flask unfollow https://relay.example.social/inbox
```
